package com.rngservers.hiddennameplates.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.rngservers.hiddennameplates.nameplate.NameplateManager;

public class Events implements Listener {
	private NameplateManager nameplate;

	public Events(NameplateManager nameplate) {
		this.nameplate = nameplate;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		nameplate.start(player);
	}
}
