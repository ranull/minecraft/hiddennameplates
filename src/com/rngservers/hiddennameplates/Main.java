package com.rngservers.hiddennameplates;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.rngservers.hiddennameplates.events.Events;
import com.rngservers.hiddennameplates.nameplate.NameplateManager;
import com.rngservers.hiddennameplates.nameplate.util.Util;

public class Main extends JavaPlugin {
	NameplateManager nameplate;
	Util util;

	@Override
	public void onEnable() {
		nameplate = new NameplateManager(this);
		util = new Util();
		this.getServer().getPluginManager().registerEvents(new Events(nameplate), this);
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			nameplate.start(player);
		}
	}

	@Override
	public void onDisable() {
		getServer().getLogger().info("Disabling plugin, showing all players");
		List<Player> onlinePlayers = util.getOnlinePlayers();
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			for (Player onlinePlayer : onlinePlayers) {
				player.showPlayer(this, onlinePlayer);
			}
		}
	}
}