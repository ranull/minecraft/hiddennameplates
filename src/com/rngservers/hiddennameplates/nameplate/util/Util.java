package com.rngservers.hiddennameplates.nameplate.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_14_R1.EntityPlayer;
import net.minecraft.server.v1_14_R1.EnumItemSlot;
import net.minecraft.server.v1_14_R1.PacketPlayOutEntity;
import net.minecraft.server.v1_14_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_14_R1.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_14_R1.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_14_R1.PacketPlayOutNamedEntitySpawn;

public class Util {
	public List<Player> getCanSee(Player player, int radius) {
		List<Entity> nearbyEntities = player.getNearbyEntities(radius, radius, radius);
		List<Player> nearbyPlayers = new ArrayList<Player>();
		for (Entity entity : nearbyEntities) {
			if (entity instanceof Player) {
				if (player.hasLineOfSight(entity)) {
					// if (hasLineOfSight(player.getLocation(), entity.getLocation())) {
					Player nearbyPlayer = ((Player) entity).getPlayer();
					nearbyPlayers.add(nearbyPlayer);
				}
			}
		}
		return nearbyPlayers;
	}

	public List<Player> getCantSee(Player player, int radius) {
		List<Entity> nearbyEntities = player.getNearbyEntities(radius, radius, radius);
		List<Player> nearbyPlayers = new ArrayList<Player>();
		for (Entity entity : nearbyEntities) {
			if (entity instanceof Player) {
				if (!player.hasLineOfSight(entity)) {
					Player nearbyPlayer = ((Player) entity).getPlayer();
					nearbyPlayers.add(nearbyPlayer);
				}
			}
		}
		return nearbyPlayers;
	}

	public List<Player> getNearbyPlayers(Player player, int radius) {
		List<Entity> nearbyEntities = player.getNearbyEntities(radius, radius, radius);
		List<Player> nearbyPlayers = new ArrayList<Player>();
		for (Entity entity : nearbyEntities) {
			if (entity instanceof Player) {
				Player nearbyPlayer = ((Player) entity).getPlayer();
				nearbyPlayers.add(nearbyPlayer);
			}
		}
		return nearbyPlayers;
	}

	public List<Player> getOnlinePlayers() {
		List<Player> online = new ArrayList<Player>();
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			online.add(player);
		}
		return online;
	}

	@SuppressWarnings({ "unused", "deprecation" })
	private boolean hasLineOfSight(Location player, Location target) {
		player.add(0, 1, 0);
		target.add(0, 1, 0);
		boolean mainResult = false;
		int acc = 12;
		int c;
		for (c = 0; c <= 360; c += 360 / acc) {
			double i = Math.tan(Math.toRadians(c)) * 0.3D * Math.sqrt(2.0D);
			double n = Math.sin(Math.toRadians(c)) * 0.3D * Math.sqrt(2.0D);
			boolean result = true;
			Location location3 = new Location(target.getWorld(), target.getX() + i, target.getY(), target.getZ() + n);
			Vector pos1 = player.toVector();
			Vector pos2 = location3.toVector();
			Vector vector = pos2.clone().subtract(pos1).normalize().multiply(0.1D);
			double distance = player.distance(location3);
			if (distance <= 50.0D) {
				for (double covered = 0.0D; covered < distance; pos1.add(vector)) {
					covered += 0.1D;
					Block block = (new Location(player.getWorld(), pos1.getX(), pos1.getY(), pos1.getZ())).getBlock();
					if (!block.isLiquid() && !block.getType().isTransparent() && block.getType().isOccluding()) {
						if (block.getType() != Material.AIR) {
							result = false;
							break;
						}
					}
				}
				mainResult = (mainResult || result);
			}
		}
		return mainResult;
	}

	public void hidePlayer(Player player, Player hider) {
		PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(
				new int[] { ((CraftPlayer) hider).getHandle().getId() });
		(((CraftPlayer) player).getHandle()).playerConnection.sendPacket(packet);
	}

	byte toByte(float yaw_pitch) {
		return (byte) (int) (yaw_pitch * 256.0F / 360.0F);
	}

	public void showPlayer(Player player, Player hider) {
		EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
		EntityPlayer entityHider = ((CraftPlayer) hider).getHandle();
		PacketPlayOutNamedEntitySpawn packet2 = new PacketPlayOutNamedEntitySpawn(entityHider);
		entityPlayer.playerConnection.sendPacket(packet2);
		PacketPlayOutEntity.PacketPlayOutEntityLook packet3 = new PacketPlayOutEntity.PacketPlayOutEntityLook(
				entityHider.getId(), toByte(hider.getLocation().getYaw()), toByte(hider.getLocation().getPitch()),
				true);
		entityPlayer.playerConnection.sendPacket(packet3);
		PacketPlayOutEntityHeadRotation packet4 = new PacketPlayOutEntityHeadRotation(entityHider,
				toByte(hider.getLocation().getYaw()));
		entityPlayer.playerConnection.sendPacket(packet4);
		PacketPlayOutEntityEquipment head = new PacketPlayOutEntityEquipment(entityHider.getId(), EnumItemSlot.HEAD,
				CraftItemStack.asNMSCopy(hider.getInventory().getHelmet()));
		entityPlayer.playerConnection.sendPacket(head);
		PacketPlayOutEntityEquipment chest = new PacketPlayOutEntityEquipment(entityHider.getId(), EnumItemSlot.CHEST,
				CraftItemStack.asNMSCopy(hider.getInventory().getChestplate()));
		entityPlayer.playerConnection.sendPacket(chest);
		PacketPlayOutEntityEquipment legs = new PacketPlayOutEntityEquipment(entityHider.getId(), EnumItemSlot.LEGS,
				CraftItemStack.asNMSCopy(hider.getInventory().getLeggings()));
		entityPlayer.playerConnection.sendPacket(legs);
		PacketPlayOutEntityEquipment feet = new PacketPlayOutEntityEquipment(entityHider.getId(), EnumItemSlot.FEET,
				CraftItemStack.asNMSCopy(hider.getInventory().getBoots()));
		entityPlayer.playerConnection.sendPacket(feet);
		PacketPlayOutEntityEquipment mainHand = new PacketPlayOutEntityEquipment(entityHider.getId(),
				EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(hider.getInventory().getItemInMainHand()));
		entityPlayer.playerConnection.sendPacket(mainHand);
		PacketPlayOutEntityEquipment offHand = new PacketPlayOutEntityEquipment(entityHider.getId(),
				EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(hider.getInventory().getItemInOffHand()));
		entityPlayer.playerConnection.sendPacket(offHand);
	}
}
