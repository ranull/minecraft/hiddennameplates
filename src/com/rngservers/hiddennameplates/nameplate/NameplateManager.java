package com.rngservers.hiddennameplates.nameplate;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.rngservers.hiddennameplates.Main;
import com.rngservers.hiddennameplates.nameplate.util.Util;

import net.md_5.bungee.api.ChatColor;

public class NameplateManager {
	private Main plugin;

	public NameplateManager(Main plugin) {
		this.plugin = plugin;
	}

	public void hideNameplate(Player player) {
		Scoreboard scoreboard = player.getScoreboard();
		Team team = scoreboard.getTeam("HiddenNameplates");
		if (team == null) {
			team = scoreboard.registerNewTeam("HiddenNameplates");
		}
		// team.setOption(Team.Option.NAME_TAG_VISIBILITY, OptionStatus.FOR_OWN_TEAM);
		Objective name = scoreboard.getObjective("name");
		if (name == null) {
			name = scoreboard.registerNewObjective("name", "dummy", "meow");
		}
		name.setDisplayName(ChatColor.RED + "e");
		team.addEntry(player.getName());
		player.setScoreboard(scoreboard);
	}

	public void armorStand(Player player) {
		List<Entity> passangers = player.getPassengers();
		for (Entity entity : passangers) {
			if (entity instanceof ArmorStand) {
				// ArmorStand armorStand = (ArmorStand) entity;
				// PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(new int[]
				// { armorStand.getEntityId() });
				// (((CraftPlayer)player).getHandle()).playerConnection.sendPacket(packet);
				return;
			}
		}

		ArmorStand newArmorStand = (ArmorStand) player.getWorld().spawnEntity(player.getLocation(),
				EntityType.ARMOR_STAND);
		newArmorStand.setCollidable(false);
		newArmorStand.setSmall(true);
		newArmorStand.setCustomName(player.getName());
		newArmorStand.setCustomNameVisible(true);
		newArmorStand.setInvulnerable(true);
		player.getPassengers().add(newArmorStand);
	}

	public void start(Player player) {
		// Map<Player, List<Player>> canSee = new HashMap<Player, List<Player>>();
		new BukkitRunnable() {
			@Override
			public void run() {
				if (!player.isOnline()) {
					return;
				}
				hideNameplate(player);
				Util util = new Util();
				int radius = 10;
				List<Player> nearbyPlayers = util.getNearbyPlayers(player, radius);
				List<Player> canSeePlayers = util.getCanSee(player, radius);
				List<Player> cantSeePlayers = util.getCantSee(player, radius);
				// player.sendMessage(ChatColor.GREEN + canSeePlayers.toString());
				// player.sendMessage(ChatColor.RED + cantSeePlayers.toString());

				List<Player> onlinePlayers = util.getOnlinePlayers();
				onlinePlayers.remove(player);

				List<Player> farAwayPlayers = new ArrayList<Player>(onlinePlayers);
				farAwayPlayers.removeAll(nearbyPlayers);

				// Packet
				// CraftPlayer craftPlayer = (CraftPlayer) player;

				// Show players up close
				for (Player seePlayer : canSeePlayers) {
					player.showPlayer(plugin, seePlayer);
					player.setSneaking(false);
//		    		 Boolean see = player.canSee(seePlayer);
//		    		 if (!see) {
//		    			 player.showPlayer(plugin, seePlayer);
//		    			 //player.setCustomNameVisible(true);
//		    		 }
				}

				// Hide players behind wall close
				for (Player seePlayer : cantSeePlayers) {
					Boolean see = player.canSee(seePlayer);
					if (see) {
						// player.hidePlayer(plugin, seePlayer);
						player.setSneaking(true);
						// armorStand(player);
						// player.setCustomName("");
						// player.setCustomNameVisible(false);
						// EntityPlayer entityPlayer = (EntityPlayer) ((CraftPlayer)
						// seePlayer).getHandle();
						// PacketPlayOutPlayerInfo packet = new
						// PacketPlayOutPlayerInfo(EnumPlayerInfoAction.UPDATE_DISPLAY_NAME,
						// entityPlayer);
						// craftPlayer.getHandle().playerConnection.sendPacket(packet);
					}
				}

				// Hide players far away
				for (Player seePlayer : farAwayPlayers) {
					Boolean see = player.canSee(seePlayer);
					if (see) {
						// player.hidePlayer(plugin, seePlayer);
						player.setSneaking(true);
						// armorStand(player);
						// player.setCustomName("");
						// player.setCustomNameVisible(false);
						// EntityPlayer entityPlayer = (EntityPlayer) ((CraftPlayer)
						// seePlayer).getHandle();
						// PacketPlayOutPlayerInfo packet = new
						// PacketPlayOutPlayerInfo(EnumPlayerInfoAction.UPDATE_DISPLAY_NAME,
						// entityPlayer);
						// craftPlayer.getHandle().playerConnection.sendPacket(packet);
					}
				}
			}
		}.runTaskTimer(plugin, 0L, 1L);
	}
}
